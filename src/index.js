import React, { Component } from 'react';
import { render } from 'react-dom'
import JSONEditor from 'jsoneditor';

require('bootstrap/dist/css/bootstrap.css')
require('./jsoneditor.min.css')

const defaultJson = require('../plot.json');

// const defaultJson = {
//   'Array': [1, 2, 3],
//   'Boolean': true,
//   'Null': null,
//   'Number': 123,
//   'Object': {'a': 'b', 'c': 'd'},
//   'String': 'Hello World'
// };

// See this file for guidance on schema validation
// streambed/shelly/filewell/static/filewell/src/schemas/utils/StylePanelSpecTestUtils.js

class JsonEditPanel extends Component {
  constructor() {
    super()
    this.state = {
      json: defaultJson
    }
  }

  componentDidMount() {
    const options = {
      onChange: () => {
        this.setState({json: this.jsonEditor.get()});
      }
    };

    this.jsonEditor = new JSONEditor(this.refs.jsoneditor, options)
    this.jsonEditor.set(this.state.json)
  }

  render () {
    return (
      <div className="row" style={{paddingTop: '50px'}}>
        <div className="col-md-6">
          <div ref="jsoneditor"/>
        </div>
        <div className="col-md-6">
          <pre><code>{JSON.stringify(this.state.json, null, 2)}</code></pre>
        </div>
      </div>
    )
  }
}

render(<div className="container"><JsonEditPanel /></div>, document.getElementById('react-app'))
